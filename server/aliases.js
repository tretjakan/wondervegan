const moduleAlias = require('module-alias')

moduleAlias.addAliases({
  "@root": __dirname,
  "@controllers": __dirname + "/controllers",
  "@routes": __dirname + "/routes",
  "@utils": __dirname + "/utils",
  "@models": __dirname + "/db",
  "@passport": __dirname + "/passport",
  "@modules": __dirname + "/modules",
  "@enums": __dirname + "/utils/enums.js"
});

moduleAlias(__dirname + '/package.json')

moduleAlias();