/**
 * Get user
 */
const Operators = require('sequelize').Op;
const { User } = require('@models');

const getUser = async query => {
  try {
    const user = await User.findOne({
      where: {
        [Operators.and]: {
          ...query,
        },
      },
    });
    return user;
  } catch (error) {
    throw new Error(error);
  }
};

module.exports = {
  getUser,
};
