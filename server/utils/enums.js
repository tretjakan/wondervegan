const ModelNames = {
  user: 'users',
  accessToken: 'accessTokens',
};

module.exports = {
  ModelNames,
};
