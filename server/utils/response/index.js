class Response {
  constructor(
    {
      // Response logic result message
      message,
      // Response data
      data,
      // Response logic result true/false
      status,
      // Response status code duplication
      code,
      // Options errors object with string error map
      errors,
      // Updated token
      newToken,
    },
  ) {
    this.message = message || '';
    this.data = data || {};
    this.status = status !== undefined ? status : true;
    this.code = code || 200;
    this.errors = errors || undefined;
    this.newToken = newToken || null;
  }

  send(res) {
    return res.status(this.code).send({
      message: this.message,
      data: this.data,
      status: this.status,
      code: this.code,
      errors: this.errors,
      newToken: this.newToken,
    });
  }
}

const errorResponse = (res, err, code) => {
  let message = '';
  console.error(err);
  if (typeof err === 'string') {
    message = err;
  } else if (err.message) {
    ({ message } = err);
  } else if (err.text) {
    message = err.text;
  }
  return new Response({
    message,
    status: false,
    code: code || 400,
  }).send(res);
};

const successResponse = (res, data, message) => new Response({
  data,
  message,
  newToken: res.isTokenUpdated && { isTokenUpdated: true, token: res.newAccessToken },
  code: 200,
  status: true,
}).send(res);

module.exports = {
  Response,
  errorResponse,
  successResponse,
};
