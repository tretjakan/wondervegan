const { User } = require('@models');
const { successResponse, errorResponse } = require('@utils/response');
const { createUserModule } = require('@modules/users');

/**
 * Create User.
 */
const createUser = async (req, res) => { 
  const {
    lastName,
    password,
    firstName,
    email,
    phoneNumber,
  } = req.body;
  if (!lastName || !password || !firstName || !email || !phoneNumber) {
    return errorResponse(res, 'Fill out all fields.', 403);
  }
  try {
    const user = await createUserModule({
      lastName,
      password,
      firstName,
      email,
      phoneNumber,
    });
    return successResponse(res, { user });
  } catch (error) {
    return errorResponse(res, error);    
  }
};


/**
 * Get list Users
 */
const getAllUsers = async (req, res) => {
  try {
    const users = await User.findAll();
    return successResponse(res, users);
  } catch (error) {
    return errorResponse(res, error);
  }
};

module.exports = {
  createUser,
  getAllUsers,
}