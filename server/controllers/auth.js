const bcrypt = require('bcrypt');
const { successResponse, errorResponse } = require('@utils/response');
const { getUser } = require('@utils/helpers/auth');
const { authenticate, loginModule, verificationModule } = require('@modules/auth');

/**
 * Login
 */
const login = async (req, res) => {
  try {
    const { email, password } = req.body;
    if (!email || !password) {
      return errorResponse(res, 'Provide name and password', 403);
    }
  
    const user = await getUser({ email, isEnabled: true });
    if (!user) {
      return errorResponse(res, 'No such user found', 401);
    }
  
    const match = await bcrypt.compare(password, user.password);
  
    if (!match) {
      return errorResponse(res, 'Password is incorrect', 401);
    }
    
    const loginData = await loginModule({ user });

    if (!loginData) {
      return errorResponse(res, 'Cannot login to the system. Please try later.', 404);
    }

    return successResponse(res, { user: loginData });
  } catch (error) {
    return errorResponse(res, error);
  }
}

/**
 * Just for test PROTECTED ROUTE
 * TODO: delete
 */
const getProt = async (req, res, next) => {
  try {
    await authenticate(req, res, next);
  } catch (error) {
    return errorResponse(res, error);
  }
  return successResponse(res, { });
}

/** enable user though email */
const verification = async (req, res) => {
  if (!req.params.token) {
    return errorResponse(res, 'Provide token.', 403);
  }
  try {
    const user = await verificationModule(req.params.token);
    return successResponse(res, { user });
  } catch (error) {
    return errorResponse(res, error);
  }
}


module.exports = {
  login,
  getProt,
  verification,
}