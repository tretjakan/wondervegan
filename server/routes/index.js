const users = require('./users');
const auth = require('./auth');

module.exports = (app) => {
  users(app);
  auth(app);
};

