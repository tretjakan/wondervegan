const { Users } = require('@controllers');

module.exports = (app) => {
  app.route('/users').get(Users.getAllUsers);
  app.route('/users/register').post(Users.createUser);
};
