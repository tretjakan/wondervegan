const { Auth } = require('@controllers');

module.exports = (app) => {
  app.route('/login').post(Auth.login);
  app.route('/login/verification/:token').get(Auth.verification);
  app.route('/protected').get(Auth.getProt);
};
