const { SEND_GRID_API_KEY, SEND_GRID_SENDER, TEMPLATE } = process.env;

const Sendgrid = require('sendgrid')(SEND_GRID_API_KEY);

const sendEmail = async ({ data, email }) => {
  const sendgridRequest = Sendgrid.emptyRequest({
    method: 'POST',
    path: '/v3/mail/send',
    body: {
      personalizations: [
        {
          to: [{ email }],
          dynamic_template_data: { ...data },
        },
      ],
      from: {
        email: SEND_GRID_SENDER,
      },
      template_id: TEMPLATE,
    },
  });
  Sendgrid.API(sendgridRequest);
}

module.exports = {
  sendEmail
};
