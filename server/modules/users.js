const bcrypt = require('bcrypt');
const { User } = require('@models');
const { tokenModule } = require('@modules/auth');
const { sendEmail } = require('@modules/email');
const saltRounds = 10;


const deleteUserModule = async query => await User.destroy({
  where: query,
});

const createUserModule = async ({
  lastName, firstName, email, password, phoneNumber,
}) => new Promise((resolve, reject) => {
  bcrypt.hash(password, saltRounds, async (err, hash) => {
    if (err) {
      return reject(err);
    }
    try {
      const user = await User.create({
        lastName, firstName, email, password: hash, phoneNumber,
      });
  
      const token = await tokenModule({ user }, 5);
      const link = `http://localhost:4200/dashboard?token=${token}`;
      await sendEmail({ data: { link }, email });
  
      return resolve(user);
    } catch (error) {
      return reject(error);
    }
  });
});

module.exports = {
  createUserModule,
  deleteUserModule,
}