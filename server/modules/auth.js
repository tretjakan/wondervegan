const moment = require('moment');
const jwt = require('jsonwebtoken');
const Operators = require('sequelize').Op;
const { passport } = require('@passport');
const { AccessTokens, User } = require('@models');
const { jwtOptions } = require('@passport/strategy');
const { getUser } = require('@utils/helpers/auth');

const { deleteUserModule } = require('./users');

const findToken = async query => await AccessTokens.findOne({
  where: query,
});

/**
 * Method for auth user. If access token is expired we would try to refresh token
 * if expiration date if OK.
 */
const authenticate = async (req, res, next) => {
  return new Promise((resolve, reject) => {
    if (!req || !req.headers || !req.headers.authorization) {
      return reject('Provide authorization token.');
    }

    passport.authenticate('jwt', async (err, user) => {
      try {
        if (err) {
          return reject(err);
        }

        if (!user) {
          const token = await findToken({
            [Operators.and]: {
              accessToken: req.headers.authorization,
              expirationAt: {
                [Operators.gte]: Date.now(),
              },
            },
          });
          if (!token) {
            return reject('Login to system, please.');
          }
          const newAccessToken = await tokenModule({ user: { userId: token.userId }, isUpdate: true });
          res.isTokenUpdated = true;
          res.newAccessToken = newAccessToken;
          return resolve();
        }

        return resolve();
      } catch (error) {
        return reject(error);
      }
    })(req, res, next);
  });
}

const loginModule = async ({ user }) => {
  if (!user) {
    throw new Error('User should be filled.');
  }

  const findToken = await AccessTokens.findOne({
    where: {
      [Operators.and]: {
        userId: user.userId,
      },
    }
  });

  const { firstName, lastName, email, phoneNumber } = user;

  const userDataPrepared = {
    firstName,
    lastName,
    email,
    phoneNumber,
  };

  if (!findToken) {
    const newAccessToken = await tokenModule({ user });
    userDataPrepared.token = newAccessToken;
    return userDataPrepared;
  }

  const compareDates = moment(findToken.expirationAt).isAfter(Date.now());

  if (!compareDates) {
    const newAccessToken = await tokenModule({ user, isUpdate: true });
    userDataPrepared.token = newAccessToken;
    return userDataPrepared;
  }

  userDataPrepared.token = findToken.accessToken;
  return userDataPrepared;
}

const tokenModule = async ({ user, isUpdate }, expiration ) => {
  if (!user) {
    return reject('User should be filled.');
  }

  const expirationDate = expiration || 20;

  const payload = { id: user.userId };
  const accessToken = jwt.sign(payload, jwtOptions.secretOrKey, { expiresIn: '12h' });

  const tokenExpiration = moment().add(expirationDate, 'days').valueOf();

  if (isUpdate) {
    /** update access token */
    await AccessTokens.update({
      accessToken,
      expirationAt: tokenExpiration,
    },
    { where: { userId: user.userId, }});

    return accessToken;
  }

  await AccessTokens.create({
    userId: user.userId,
    accessToken,
    expirationAt: tokenExpiration,
  });

  return accessToken;
};

const verificationModule = async (token) => {
  const accessToken = await findToken({
    [Operators.and]: {
      accessToken: token,
    },
  });

  if (!accessToken) {
    throw new Error('Активация НЕ прошла. Пройдите регистрацию заново.');
  }

  const user = await getUser({ userId: accessToken.userId });

  if (user.isEnabled) {
    throw new Error('Пользователь был активирован.');
  }

  const compareDates = moment(accessToken.expirationAt).isAfter(Date.now());
  if (!compareDates) {
    await deleteUserModule({
      userId: accessToken.userId,
    });
    throw new Error('Срок ссылки для активации истек. Пройдите регистрацию заново.');
  }

  const newUser = await User.update({
    isEnabled: true,
  },
  { where: { userId: accessToken.userId } });
  return newUser;
}

module.exports = {
  loginModule,
  authenticate,
  tokenModule,
  verificationModule,
};