const passport = require('passport');
const { strategy } = require('./strategy');

passport.use(strategy);

module.exports = {
  passport,
};
