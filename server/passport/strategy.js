const passportJWT = require('passport-jwt');
const { getUser } = require('@utils/helpers/auth');

/** Passport strategy */
const ExtractJwt = passportJWT.ExtractJwt;
const JwtStrategy = passportJWT.Strategy;

/**
 * Config
 */
const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromHeader('authorization'),
  secretOrKey: 'wowwow',
};


const strategy = new JwtStrategy(jwtOptions, async (jwt_payload, next) => {
  try {
    const user = await getUser({ userId: jwt_payload.id, isEnabled: true });
    if (user) {
      next(null, user);
    } else {
      next(null, false);
    }
  } catch (error) {
    next(null, false);
  }
});

module.exports = {
  strategy,
  jwtOptions,
};

/** Passport strategy ---END--- */