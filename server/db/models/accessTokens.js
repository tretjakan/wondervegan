const { ModelNames } = require('@enums');

module.exports = {
  accessTokenSchema: (sequelize, Sequelize, User) => {
    const AccessTokens = sequelize.define(ModelNames.accessToken, {
      userId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'CASCADE',
        references: {
          model: User,
          key: 'userId'
        },
        unique: true,
      },
      accessToken: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
      },
      expirationAt: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW,
      },
    });
    
    AccessTokens.sync()
      .then(() => console.log('AccessTokens table created successfully'))
      .catch(err => console.error(err));

    return AccessTokens;
  }
};

