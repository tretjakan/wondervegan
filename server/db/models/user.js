const { ModelNames } = require('@enums');

module.exports = {
  userSchema: (sequelize, Sequelize) => {
    const User = sequelize.define(ModelNames.user, {
      firstName: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      lastName: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      password: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      email: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: {
          args: true,
          msg: 'Email address already in use!'
        },
        validate: {
          isEmail:true
        },
      },
      userId: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      phoneNumber: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
      },
      createdAt: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
      isEnabled: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
    });
    
    User.sync()
      .then(() => console.log('User table created successfully'))
      .catch(err => console.error(err));

    return User;
  }
};

