const { userSchema } = require('./user');
const { accessTokenSchema } = require('./accessTokens');

module.exports = {
  userSchema,
  accessTokenSchema,
};