const Sequelize = require('sequelize');

const { userSchema, accessTokenSchema } = require('./models');

const sequelize = new Sequelize(JSON.parse(process.env.DB_CONNECT));

sequelize
  .authenticate()
  .then(() => console.log('Connection has been established successfully.'))
  .catch(err => console.error('Unable to connect to the database: ', err));

const User = userSchema(sequelize, Sequelize);
const AccessTokens = accessTokenSchema(sequelize, Sequelize, User);

module.exports = {
  User,
  AccessTokens,
};