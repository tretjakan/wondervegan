require('./aliases');
const fs = require('fs');
const cors = require('cors');

if (fs.existsSync(__dirname + '/.env')) {
  console.log('.env found, parsing...');
  require('dotenv-safe').config({
    path: __dirname + '/.env',
    example: __dirname + '/.env.sample',
  });
} else {
  console.log('.ENV file not found!');
}

const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.use(cors());
app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: true }));

app.use(require('@passport').passport.initialize());

/**
 * Connect to db
 */
require('@models');

/**
 * Routes
 */
require('@routes')(app);

app.listen(process.env.PORT, () => console.log(`Express is running on port 3000`));