import { NgModule } from '@angular/core';

import { Routes, RouterModule } from '@angular/router';
import { CategoriesComponent } from '@pages/categories/categories.component';
import { ProductComponent } from '@pages/product/product.component';
import { ProfileComponent } from '@pages/profile/profile.component';
import { ProfileGuard } from '@pages/profile/profile.guard';

const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: 'dashboard', loadChildren: './pages/dashboard/dashboard.module#DashboardModule' },
  { path: 'goods', component: CategoriesComponent },
  { path: 'goods/:id', loadChildren: './pages/goods/goods.module#GoodsModule' },
  { path: 'goods/:id/:productId', component: ProductComponent },
  { path: 'profile', component: ProfileComponent, canActivate: [ProfileGuard] },
  { path: '**', redirectTo: 'dashboard' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  providers: [ProfileGuard],
  exports: [RouterModule],
})

export class AppRoutingModule { }
