import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { reducers, metaReducers } from '@store/reducers';
import { NxModule } from '@nrwl/nx';

import { ProductsEffects } from '@store/products/products.effects';
import { CategoriesEffects } from '@store/categories/category.effects';
import { AuthEffects } from '@store/auth/auth.effects';

@NgModule({
  imports: [
    CommonModule,
    NxModule.forRoot(),
    StoreModule.forRoot(reducers, { metaReducers }),
    StoreDevtoolsModule.instrument({ maxAge: 10 }),
    EffectsModule.forRoot([
      ProductsEffects,
      CategoriesEffects,
      AuthEffects,
    ]),
  ],
  declarations: []
})
export class StateModule { }
