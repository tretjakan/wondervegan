import {
  ActionReducerMap,
  MetaReducer
} from '@ngrx/store';
import { environment } from '@env/environment';
import { productsReducer, ProductsState } from '@store/products/products.reducer';
import { categoriesReducer, CategoriesState } from '@store/categories/category.reducer';
import { authReducer, AuthState } from '@store/auth/auth.reducer';
import { cartReducer, CartState } from '@store/cart/cart.reducer';

export interface State {
  products: ProductsState;
  categories: CategoriesState;
  auth: AuthState;
  cart: CartState;
}

export const reducers: ActionReducerMap<State> = {
  products: productsReducer,
  categories: categoriesReducer,
  auth: authReducer,
  cart: cartReducer,
};


export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];
