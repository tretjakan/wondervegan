import { Injectable } from '@angular/core';
import { select, Store, createFeatureSelector, createSelector } from '@ngrx/store';
import { Observable } from 'rxjs';
import * as fromAuth from './auth.reducer';
import { Registration, TryAgainRegistration, Login, LoginOut, Validate, OpenDialog } from './auth.actions';
import { User } from '@services/auth/auth.model';


/**
 * Select auth state
 */
const selectAuthState
  = createFeatureSelector<fromAuth.AuthState>('auth');

/**
 * Select user from state
 */
export const selectUser = createSelector(
  selectAuthState,
  state => state.user,
);

/**
 * is Loading select
 */
const selectLoading = createSelector(
  selectAuthState,
  state => state.isLoading,
);

/**
 * Error select
 */
const selectError = createSelector(
  selectAuthState,
  state => state.isError,
);

const selectSuccess = createSelector(
  selectAuthState,
  state => state.isRegisteredSuccess,
);

export const selectLoggedIn = createSelector(
  selectAuthState,
  state => state.isLoggedIn,
);

const selectValidation = createSelector(
  selectAuthState,
  state => state.isValidationSuccess,
);

@Injectable({
  providedIn: 'root'
})
export class AuthFacade {
  user$: Observable<User>;
  isLoading$: Observable<boolean>;
  isError$: Observable<boolean>;
  isRegisteredSuccess$: Observable<boolean>;
  isLoggedIn$: Observable<boolean>;
  isValidationSuccess$: Observable<boolean>;

  constructor(private store: Store<fromAuth.AuthState>) {
    this.user$ = store.pipe(select(selectUser));
    this.isLoading$ = store.pipe(select(selectLoading));
    this.isError$ = store.pipe(select(selectError));
    this.isRegisteredSuccess$ = store.pipe(select(selectSuccess));
    this.isLoggedIn$ = store.pipe(select(selectLoggedIn));
    this.isValidationSuccess$ = store.pipe(select(selectValidation));
  }

  public registration(user: User) {
    this.store.dispatch(new Registration(user));
  }

  public tryAgainRegistration() {
    this.store.dispatch(new TryAgainRegistration());
  }

  public login(user: User, ref: any) {
    this.store.dispatch(new Login(user, ref));
  }

  public logOut() {
    this.store.dispatch(new LoginOut());
  }

  public validate(token: string, openSnackbar: any) {
    const user = localStorage.getItem('user');
    if (user) {
      return;
    }
    this.store.dispatch(new Validate(token, openSnackbar));
  }

  public openAuth(data: any) {
    this.store.dispatch(new OpenDialog(data));
  }
}
