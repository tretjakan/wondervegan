import { Action } from '@ngrx/store';
import { User } from '@services/auth/auth.model';

export enum AuthActionTypes {
  Registration = '[auth] registration started',
  RegistrationSuccedeed = '[auth] registration success',
  RegistrationFailed = '[auth] registration failed',
  TryAgainRegistration = '[auth] registration try again',
  Login = '[auth] login',
  LoginFailed = '[auth] login failed',
  LoginSuccedeed = '[auth] login success',
  LoginOut = '[auth] login out',
  Validate = '[auth] validate',
  ValidateSuccedeed = '[auth] validate success',
  ValidateFailed = '[auth] validate fail',
  OpenDialog = '[auth] open dialog',
}

/**
 * Registration action
 */
export class Registration implements Action {
  readonly type = AuthActionTypes.Registration;
  constructor(public payload: User) {}
}

export class TryAgainRegistration implements Action {
  readonly type = AuthActionTypes.TryAgainRegistration;
}

/**
 * Login
 */
export class Login implements Action {
  readonly type = AuthActionTypes.Login;
  constructor(public payload: User, public ref: any) {}
}

/** LOGout */
export class LoginOut implements Action {
  readonly type = AuthActionTypes.LoginOut;
}

export class ValidateFailed implements Action {
  readonly type = AuthActionTypes.ValidateFailed;
}

export class OpenDialog implements Action {
  readonly type = AuthActionTypes.OpenDialog;
  constructor(private payload: any) {
    payload.ref.open(payload.component, payload.settings);
  }
}

export class ValidateSuccedeed implements Action {
  readonly type = AuthActionTypes.ValidateSuccedeed;
}

/**
 * With Effects
 */

export class RegistrationSuccedeed implements Action {
  readonly type = AuthActionTypes.RegistrationSuccedeed;
  constructor(public payload: User) { }
}

export class RegistrationFailed implements Action {
  readonly type = AuthActionTypes.RegistrationFailed;
}


export class LoginFailed implements Action {
  readonly type = AuthActionTypes.LoginFailed;
}

export class LoginSuccedeed implements Action {
  readonly type = AuthActionTypes.LoginSuccedeed;
  constructor(public payload: User) { }
}

export class Validate implements Action {
  readonly type = AuthActionTypes.Validate;
  constructor(public payload: string, public openSnackbar: any) { }
}


/**
 * Events
 */

export type AuthActions = Registration
| RegistrationSuccedeed
| RegistrationFailed
| Login
| LoginFailed
| LoginSuccedeed
| LoginOut
| Validate
| ValidateFailed
| ValidateSuccedeed
| OpenDialog;
