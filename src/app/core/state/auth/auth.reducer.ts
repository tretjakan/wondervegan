import { User } from '@services/auth/auth.model';

import { AuthActionTypes as Actions } from './auth.actions';

/**
 * Define State
 */

const getUser = (): User => {
  const get = localStorage.getItem('user');
  if (get) {
    const {
      firstName, lastName, email, token, phoneNumber
    } = JSON.parse(get);
    return {
      firstName,
      lastName,
      email,
      phoneNumber,
      token,
    };
  }
  return null;
};

export interface AuthState {
  user: User;
  isLoading: boolean;
  isError: boolean;
  isRegisteredSuccess: boolean;
  isLoggedIn: boolean;
  isValidationSuccess: boolean;
}

const defaultUser = {
  email: '',
  firstName: '',
  lastName: '',
  token: null,
};

export const initialState: AuthState = {
  user: getUser() || defaultUser,
  isLoading: false,
  isError: false,
  isRegisteredSuccess: false,
  isLoggedIn: Boolean(getUser()),
  isValidationSuccess: null,
};

export function authReducer(
  state = initialState, action): AuthState {
    const { type, payload } = action;
    switch (type) {
      case Actions.RegistrationSuccedeed: {
        return {
          ...state,
          user: payload,
          isLoading: false,
          isError: false,
          isRegisteredSuccess: true,
        };
      }
      case Actions.Login:
      case Actions.Registration: {
        return {
          ...state,
          isLoading: true,
        };
      }
      case Actions.RegistrationFailed: {
        return {
          ...state,
          isLoading: false,
          isError: true,
          isRegisteredSuccess: false,
        };
      }
      case Actions.TryAgainRegistration: {
        return {
          ...state,
          isError: false,
          isRegisteredSuccess: false,
        };
      }
      case Actions.LoginSuccedeed: {
        return {
          ...state,
          user: payload,
          isLoading: false,
          isLoggedIn: true,
          isRegisteredSuccess: false,
        };
      }
      case Actions.LoginFailed: {
        return {
          ...state,
          isLoading: false,
        };
      }
      case Actions.LoginOut: {
        localStorage.removeItem('user');
        return {
          ...state,
          user: defaultUser,
          isLoggedIn: false,
        };
      }
      case Actions.ValidateFailed: {
        return {
          ...state,
          isValidationSuccess: false,
        };
      }
      case Actions.ValidateSuccedeed: {
        return {
          ...state,
          isValidationSuccess: true,
        };
      }
      case Actions.OpenDialog: {
        return initialState;
      }
      default: return state;
    }
}

