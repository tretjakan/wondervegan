import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/nx';
import { map } from 'rxjs/operators';
import { AuthService } from '@services/auth/auth.service';
import { Response } from '@services/auth/auth.model';

import {
  Registration, RegistrationSuccedeed, RegistrationFailed, AuthActionTypes,
  Login,
  LoginSuccedeed,
  LoginFailed,
  Validate,
  ValidateFailed,
  ValidateSuccedeed,
} from './auth.actions';
import { AuthState } from './auth.reducer';
import { State } from '@store/reducers';


@Injectable({providedIn: 'root'})
export class AuthEffects {
  @Effect() registration$ = this.dataPersistence
    .fetch(AuthActionTypes.Registration, {
      run: (action: Registration, state: AuthState) => {
        return this.authService.registration(action.payload)
          .pipe(
            map((res: Response) => new RegistrationSuccedeed(res.data.user))
          );
      },
      onError: (error) => {
        console.error(error);
        return new RegistrationFailed();
      },
  });

  @Effect() login$ = this.dataPersistence
    .fetch(AuthActionTypes.Login, {
      run: (action: Login, state: AuthState) => {
        return this.authService.login(action.payload)
          .pipe(
            map((res: Response) => {
              if (res.data) {
                localStorage.setItem('user', JSON.stringify(res.data.user));
              }
              action.ref.close();
              return new LoginSuccedeed(res.data.user);
            })
          );
      },
      onError: (error) => {
        console.error(error);
        return new LoginFailed();
      },
  });

  @Effect() validate$ = this.dataPersistence
  .fetch(AuthActionTypes.Validate, {
    run: (action: Validate) => {
      return this.authService.validate(action.payload)
        .pipe(
          map((res) => {
            action.openSnackbar('Вы успешно подтвердили регистрацию.', 'Закрыть');
            return new ValidateSuccedeed();
          })
        );
    },
    onError: (action: Validate, error) => {
      action.openSnackbar(error.error.message, 'Закрыть');
      return new ValidateFailed();
    },
});

  constructor(
    private actions$: Actions,
    private dataPersistence: DataPersistence<AuthState>,
    private authService: AuthService
  ) {}
}
