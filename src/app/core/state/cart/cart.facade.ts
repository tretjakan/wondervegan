import { Injectable } from '@angular/core';
import { Cart } from '@store/models';
import { select, Store, createFeatureSelector, createSelector } from '@ngrx/store';
import { Observable } from 'rxjs';
import * as fromCart from './cart.reducer';
import { AddToCart, Delete, IncreaseCount, ReduceCount } from './cart.actions';
import { Product } from '@services/products/products.model';

const selectCartState
  = createFeatureSelector<fromCart.CartState>('cart');

const selectAllGoods = createSelector(
  selectCartState,
  fromCart.getData,
);

const selectTotal = createSelector(
  selectCartState,
  state => state.total,
);

const selectCartCount = createSelector(
  selectCartState,
  state => state.count,
);


@Injectable({
  providedIn: 'root'
})
export class CartFacade {
  cart$: Observable<Product[]>;
  total$: Observable<number>;
  cartCount$: Observable<any>;

  constructor(private store: Store<fromCart.CartState>) {
    this.cart$ = store.pipe(
      select(selectAllGoods),
    );
    this.total$ = store.pipe(
      select(selectTotal),
    );
    this.cartCount$ = store.pipe(
      select(selectCartCount),
    );
  }

  addGood(payload: Product) {
    this.store.dispatch(new AddToCart(payload));
  }

  deleteGood(payload: Cart) {
    this.store.dispatch(new Delete(payload));
  }

  increaseCount(id: number) {
    this.store.dispatch(new IncreaseCount(id));
  }

  reduceCount(id: number) {
    this.store.dispatch(new ReduceCount(id));
  }
}
