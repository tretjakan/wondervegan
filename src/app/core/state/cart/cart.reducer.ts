import * as actions from './cart.actions';
import { Product } from '@services/products/products.model';
import { Count } from '@shared/enums';

/**
 * Define State
 */

export interface CartState {
  data: Product[];
  total: number;
  count: any;
}

export const initialState: CartState = {
  data: [],
  total: 0,
  count: {},
};


export function cartReducer(
  state = initialState, action): CartState {
    const { type, payload } = action;
    switch (type) {
      case actions.CartActionTypes.AddToCart: {
        const newState = {
          ...state,
          total: state.total + 1,
        };
        const currentCount = state.count[payload.productId];
        if (currentCount) {
          newState.count = {
            ...state.count,
            [payload.productId]: {
              ...state.count[payload.productId],
              count: state.count[payload.productId].count + 1,
            }
          };
        } else {
          newState.count = {
            ...state.count,
            [payload.productId]: {
              productId: payload.productId,
              count: 1,
            },
          };
        }
        if (!(state.data.find(item => item.productId === payload.productId))) {
          newState.data = [
            ...state.data,
            payload,
          ];
        }

        return newState;
      }
      case actions.CartActionTypes.Delete: {
        const newCount = {};
        for (const [key, value] of Object.entries(state.count)) {
          if (parseInt(key, 10) !== payload.id) {
            newCount[key] = value;
          }
        }
        return {
          ...state,
          data: state.data.filter(item => item.productId !== payload.id),
          count: newCount,
          total: state.total - 1,
        };
      }
      case actions.CartActionTypes.ReduceCount: {
        if (state.count[payload].count <= Count.Min) {
          return state;
        }
        return {
          ...state,
          count: {
            ...state.count,
            [payload]: {
              ...state.count[payload],
              count: state.count[payload].count - 1
            },
          }
        };
      }
      case actions.CartActionTypes.IncreaseCount: {
        if (state.count[payload].count >= Count.Max) {
          return state;
        }
        return {
          ...state,
          count: {
            ...state.count,
            [payload]: {
              ...state.count[payload],
              count: state.count[payload].count + 1
            },
          }
        };
      }
      default: return state;
    }
}


export const getData = (state: CartState) => state.data;
