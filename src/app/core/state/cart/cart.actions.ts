import { Action } from '@ngrx/store';
import { Cart } from '@store/models';
import { Product } from '@services/products/products.model';

export enum CartActionTypes {
  AddToCart = '[cart] add to cart',
  Delete = '[cart] delete cart',
  IncreaseCount = '[cart] increase count',
  ReduceCount = '[cart] reduce count',
}

export class AddToCart implements Action {
  readonly type = CartActionTypes.AddToCart;
  constructor(public payload: Product) {}
}

export class Delete implements Action {
  readonly type = CartActionTypes.Delete;
  constructor(public payload: Cart) { }
}

export class IncreaseCount implements Action {
  readonly type = CartActionTypes.IncreaseCount;
  constructor(public payload: number) { }
}

export class ReduceCount implements Action {
  readonly type = CartActionTypes.ReduceCount;
  constructor(public payload: number) { }
}

/**
 * Events
 */

export type ProjectsActions = AddToCart
| Delete
| IncreaseCount
| ReduceCount;
