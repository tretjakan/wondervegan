import { Pagination } from '@services/categories/categories.model';
import { Product } from '@services/products/products.model';

export interface LoadProduct {
  pagination: Pagination;
  id: number;
  filters?: Array<string> | number[];
  priceLimits?: Array<number> | number[];
}

export interface LoadProductAction {
  type: string;
  payload: LoadProduct;
}

export interface InitProductAction {
  payload?: number;
  type: string;
}

export interface Filter {
  id: number;
  name: string;
}

export interface TotalFilter {
  filters: Filter[];
  total: number;
}

export interface TotalFilterRes {
  filterId: number;
  name: string;
  total: number;
}

export interface Total {
  filters: Filter[];
  total: number;
  data: Product[];
}

export interface PriceFilter {
  max: number;
  min: number;
}

export interface Option {
  id: number;
  value: string;
}

export interface Cart {
  product: Product;
}

export interface CartCount {
  productId: number;
  count: number;
}
