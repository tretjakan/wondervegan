import { Category } from '@services/categories/categories.model';

import * as actions from './category.actions';

/**
 * Define State
 */

export interface CategoriesState {
  selectedCategoryId: number | null;
  data: Category[];
}

export const initialState: CategoriesState = {
  selectedCategoryId: null,
  data: [],
};

export function categoriesReducer(
  state = initialState, action): CategoriesState {
    const { type, payload } = action;
    switch (type) {
      case actions.CategoriesActionTypes.SelectCategory: {
        return {
          ...state,
          selectedCategoryId: payload,
        };
      }
      case actions.CategoriesActionTypes.CategoryLoaded: {
        return {
          ...state,
          data: payload.data,
        };
      }
      default: return state;
    }
}

/**
 * Selectors
 */

export const getSelectedProductId = (state: CategoriesState) => state.selectedCategoryId;


export const getData = (state: CategoriesState) => state.data;
