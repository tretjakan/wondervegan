import { Action } from '@ngrx/store';
import { Category } from '@services/categories/categories.model';

export enum CategoriesActionTypes {
  SelectCategory = '[category] Select category',

  LoadCategory = '[category] load category',
  CategoryLoaded = '[category] category loaded',

}

export class SelectCategory implements Action {
  readonly type = CategoriesActionTypes.SelectCategory;
  constructor(public payload: Category) {}
}

/**
 * With Effects
 */

export class LoadCategories implements Action {
  readonly type = CategoriesActionTypes.LoadCategory;
}

export class CategoriesLoaded implements Action {
  readonly type = CategoriesActionTypes.CategoryLoaded;
  constructor(public payload: Category[]) { }
}

/**
 * Events
 */

export type ProjectsActions = SelectCategory
| LoadCategories
| CategoriesLoaded;
