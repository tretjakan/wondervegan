import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/nx';
import { map } from 'rxjs/operators';

import { CategoriesActionTypes, LoadCategories, CategoriesLoaded } from './category.actions';
import { CategoriesState } from './category.reducer';
import { CategoriesService } from '@services/categories/categories.service';
import { Category } from '@services/categories/categories.model';

@Injectable({providedIn: 'root'})
export class CategoriesEffects {
  @Effect() loadCategories$ = this.dataPersistence
    .fetch(CategoriesActionTypes.LoadCategory, {
      run: (action: LoadCategories, state: CategoriesState) => {
        return this.categoriesService.getCategories()
          .pipe(
            map((res: Category[]) => new CategoriesLoaded(res))
          );
      },
      onError: (error) => console.error(error),
  });

  constructor(
    private actions$: Actions,
    private dataPersistence: DataPersistence<CategoriesState>,
    private categoriesService: CategoriesService
  ) {}
}
