import { Injectable } from '@angular/core';
import { select, Store, createFeatureSelector, createSelector } from '@ngrx/store';
import { Observable } from 'rxjs';
import * as fromCategories from './category.reducer';
import { LoadCategories, SelectCategory } from './category.actions';
import { Category } from '@services/categories/categories.model';

const selectCategoriesState
  = createFeatureSelector<fromCategories.CategoriesState>('categories');

const selectAllCategories = createSelector(
  selectCategoriesState,
  fromCategories.getData,
);

const emptyCategory: Category = {
  categoryId: null,
  category: '',
  alias: '',
};

const selectCurrentCategoryId = createSelector(
  selectCategoriesState,
  fromCategories.getSelectedProductId,
);

const selectCurrentCategory = createSelector(
  selectCategoriesState,
  selectCurrentCategoryId,
  (data, id) => id ? data[id] : emptyCategory,
);


@Injectable({
  providedIn: 'root'
})
export class CategoriesFacade {
  categories$: Observable<Category[]>;
  currentCategory$: Observable<Category>;

  constructor(private store: Store<fromCategories.CategoriesState>) {
    this.categories$ = store.pipe(
      select(selectAllCategories),
    );
    this.currentCategory$ = store.pipe(select(selectCurrentCategory));
  }

  getCategories() {
    this.store.dispatch(new LoadCategories());
  }

  selectCategory(category) {
    this.store.dispatch(new SelectCategory(category.categoryId));
  }
}
