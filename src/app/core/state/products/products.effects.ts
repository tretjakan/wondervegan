import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/nx';
import { map } from 'rxjs/operators';

import { CategoriesService } from '@services/categories/categories.service';
import { ProductSort } from '@shared/utils/products-sort';
import { Product, ProductResponse } from '@services/products/products.model';
import { ProductsService } from '@services/products/products.service';
import { ProductsLoaded, ProductsActionTypes, ProductsInitiated, FetchProductSuccess } from './products.actions';
import { ProductsState } from './products.reducer';
import { LoadProductAction, InitProductAction } from '../models';
import { State } from '../reducers';

@Injectable({ providedIn: 'root' })
export class ProductsEffects {
  @Effect() loadProducts$ = this.dataPersistence
    .fetch(ProductsActionTypes.LoadProducts, {
      run: (action: LoadProductAction, state: any) => {
        return this.categoriesService.getProductsByCategory({
          ...action.payload,
          filters: state.products.selectedFilters.filters,
          priceLimits: state.products.selectedFilters.prices,
        })
          .pipe(
            map((res: any) => {
              if (state.products.variant) {
                ProductSort.sortByPrice(res.data, state.products.variant);
              }
              return new ProductsLoaded(res);
            })
          );
      },
      onError: (error) => console.error(error),
  });

  @Effect() initProducts$ = this.dataPersistence
    .fetch(ProductsActionTypes.ProductInit, {
      run: (action: InitProductAction, state: ProductsState) => {
        return this.productsService.getTotal(action.payload)
          .pipe(
            map((res) => new ProductsInitiated(res))
          );
      },
      onError: (error) => console.error(error),
});

@Effect() fetchProduct$ = this.dataPersistence
.fetch(ProductsActionTypes.FetchProductStart, {
  run: (action: InitProductAction, state: ProductsState) => {
    return this.productsService.getProduct(action.payload)
      .pipe(
        map((res) => new FetchProductSuccess(res))
      );
  },
  onError: (error) => console.error(error),
});

  constructor(
    private actions$: Actions,
    private dataPersistence: DataPersistence<ProductsState>,
    private categoriesService: CategoriesService,
    private productsService: ProductsService,
  ) { }
}
