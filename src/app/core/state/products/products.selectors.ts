import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromProducts from './products.reducer';


export const selectProductsState
  = createFeatureSelector<fromProducts.ProductsState>('products');

export const selectAllProducts = createSelector(
  selectProductsState,
  fromProducts.getAll,
);

export const selectCurrentProduct = createSelector(
  selectProductsState,
  (state) => state.selectedProduct,
);

export const selectTotal = createSelector(
  selectProductsState,
  state => state.total,
);

export const selectFilters = createSelector(
  selectProductsState,
  state => state.filters,
);

export const selectMinPrice = createSelector(
  selectProductsState,
  state => state.minPrice,
);

export const selectMaxPrice = createSelector(
  selectProductsState,
  state => state.maxPrice,
);
