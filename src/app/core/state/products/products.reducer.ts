import { Product } from '@services/products/products.model';
import * as actions from './products.actions';
import { Filter } from '../models';

export const initialProducts: Product[] = [
  {
    productId: null,
    name: '',
    description: '',
    imageUrl: '',
    price: '',
    manufacturer: '',
  },
];

/**
 * Define State
 */
export interface ProductsState {
  selectedProduct: Product;
  data: Product[];
  total: number;
  filters: Filter[];
  minPrice: number;
  maxPrice: number;
  selectedFilters: {
    prices: Array<number>,
    filters: Array<number>,
  };
  variant: number;
}

export const initialState: ProductsState = {
  selectedProduct: null,
  data: [],
  total: 0,
  filters: [],
  minPrice: null,
  maxPrice: null,
  selectedFilters: {
    prices: null,
    filters: [],
  },
  variant: null,
};

export function productsReducer(
  state = initialState, action): ProductsState {
    const { type, payload, variant } = action;
    switch (type) {
      case actions.ProductsActionTypes.ProductLoaded: {
        return {
          ...state,
          data: payload.data,
          total: payload.total,
          selectedProduct: null,
        };
      }
      case actions.ProductsActionTypes.ProductInitiated: {
        return {
          ...state,
          filters: payload.filters,
          minPrice: payload.priceValues.min,
          maxPrice: payload.priceValues.max,
        };
      }
      case actions.ProductsActionTypes.FetchProduct: {
        return {
          ...state,
          selectedProduct: payload.data,
        };
      }
      case actions.ProductsActionTypes.Prices: {
        return {
          ...state,
          selectedFilters: {
            ...state.selectedFilters,
            prices: payload,
          },
        };
      }
      case actions.ProductsActionTypes.Filters: {
        return {
          ...state,
          selectedFilters: {
            ...state.selectedFilters,
            filters: payload,
          },
        };
      }
      case actions.ProductsActionTypes.SortProducts:
        return {
          ...state,
          data: payload,
          variant,
        };
      default: return state;
    }
}

/**
 * Selectors
 */

export const getAll = (state: ProductsState) => state.data;


