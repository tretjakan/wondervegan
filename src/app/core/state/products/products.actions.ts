import { Action } from '@ngrx/store';
import { Product } from '@services/products/products.model';
import { LoadProduct, Filter, Total } from '../models';


export enum ProductsActionTypes {
  LoadProducts = '[products] load products',
  ProductLoaded = '[products] products loaded',
  ProductInit = '[products] products init',
  ProductInitiated = '[products] products initiated',

  FetchProductStart = '[product] start load',
  FetchProduct = '[product] load',

  Prices = '[products] prices',
  Filters = '[products] filters',
  SortProducts = '[products] sort',
}

export class WritePriceToState implements Action {
  readonly type = ProductsActionTypes.Prices;
  constructor(public payload: any) {}
}

export class WriteFiltersToState implements Action {
  readonly type = ProductsActionTypes.Filters;
  constructor(public payload: any) {}
}

export class SortProducts implements Action {
  readonly type = ProductsActionTypes.SortProducts;
  constructor(
    public payload: Product[],
    public variant: number,
  ) {}
}


/**
 * With Effects
 */

export class LoadProducts implements Action {
  readonly type = ProductsActionTypes.LoadProducts;
  constructor(
    public payload: LoadProduct,
  ) { }
}

export class ProductsLoaded implements Action {
  readonly type = ProductsActionTypes.ProductLoaded;
  constructor(public payload: Product[]) { }
}

export class InitProducts implements Action {
  readonly type = ProductsActionTypes.ProductInit;
  constructor(public payload: number) { }
}

export class ProductsInitiated implements Action {
  readonly type = ProductsActionTypes.ProductInitiated;
  constructor(public payload: Total) { }
}


export class FetchProductSuccess implements Action {
  readonly type = ProductsActionTypes.FetchProduct;
  constructor(public payload: Product) { }
}

export class FetchProductStart implements Action {
  readonly type = ProductsActionTypes.FetchProductStart;
  constructor(public payload: number) { }
}

/**
 * Events
 */

export type ProjectsActions = LoadProducts
| ProductsLoaded
| InitProducts
| ProductsInitiated
| FetchProductStart
| FetchProductSuccess
| WritePriceToState
| WriteFiltersToState
| SortProducts;
