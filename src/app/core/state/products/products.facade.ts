import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Product } from '@services/products/products.model';
import { ProductSort } from '@shared/utils/products-sort';
import * as selectors from './products.selectors';
import {
  LoadProducts, InitProducts, FetchProductStart, WritePriceToState, WriteFiltersToState, SortProducts,
} from './products.actions';
import { LoadProduct, Filter } from '../models';
import { ProductsState } from './products.reducer';

@Injectable({
  providedIn: 'root'
})
export class ProductsFacade {
  products$: Observable<Product[]>;
  currentProduct$: Observable<Product>;
  total$: Observable<number>;
  filters$: Observable<Filter[]>;
  maxPrice$: Observable<number>;
  minPrice$: Observable<number>;

  constructor(private store: Store<ProductsState>) {
    this.products$ = store.pipe(
      select(selectors.selectAllProducts),
    );
    this.currentProduct$ = store.pipe(select(selectors.selectCurrentProduct));
    this.total$ = store.pipe(select(selectors.selectTotal));
    this.filters$ = store.pipe(select(selectors.selectFilters));
    this.maxPrice$ = store.pipe(select(selectors.selectMaxPrice));
    this.minPrice$ = store.pipe(select(selectors.selectMinPrice));
  }

  getProducts(payload: LoadProduct) {
    this.store.dispatch(new LoadProducts({
      id: payload.id,
      pagination: payload.pagination,
      filters: payload.filters,
      priceLimits: payload.priceLimits,
    }));
  }

  getTotal(id: number) {
    this.store.dispatch(new InitProducts(id));
  }

  fetchProduct(id: number) {
    this.store.dispatch(new FetchProductStart(id));
  }

  writePriceToState(payload: any) {
    this.store.dispatch(new WritePriceToState(payload));
  }

  writeFiltersToState(payload: any) {
    this.store.dispatch(new WriteFiltersToState(payload));
  }

  sortProducts(payload: Product[], type: number) {
    if (type) {
      ProductSort.sortByPrice(payload, type);
    }
    this.store.dispatch(new SortProducts(payload, type));
  }
}
