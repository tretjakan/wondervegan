import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { options } from '@env/environment';
import { Product } from './products.model';
import { Total } from '@store/models/models';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  constructor(
    private http: HttpClient,
  ) { }

  getList() {
    return this.http.get<Product[]>(`${options.apiUri}/products`);
  }

  getTotal(id: number) {
    return this.http.get<Total>(`${options.apiUri}/products/total/${id}`);
  }

  getProduct(id: number) {
    return this.http.get<Product>(`${options.apiUri}/product/${id}`);
  }
}
