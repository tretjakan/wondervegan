export interface Product {
  productId: number;
  name: string;
  description: string;
  imageUrl?: string;
  price: string;
  manufacturer: string;
}

export interface ProductResponse {
  data: Product[];
}
