import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { options } from '@env/environment';
import { Category } from './categories.model';
import { Product } from '../products/products.model';
import { LoadProduct } from '@store/models/models';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {
  constructor(
    private http: HttpClient,
  ) { }

  getCategories() {
    return this.http.get<Category[]>(`${options.apiUri}/categories`);
  }

  getProductsByCategory(payload: LoadProduct) {
    const params = new HttpParams({
      fromObject: {
        perPage: payload.pagination.perPage,
        page: payload.pagination.page,
        filters: JSON.stringify(payload.filters || []),
        priceLimits: JSON.stringify(payload.priceLimits) || null,
      },
    });
    return this.http.get<Product[]>(`${options.apiUri}/categories/${payload.id}`, { params });
  }
}
