export interface Category {
  categoryId: number;
  category: string;
  alias: string;
}

export interface Pagination {
  page: string;
  perPage: string;
  id?: number;
}
