export interface User {
  email: string;
  password?: string;
  firstName?: string;
  lastName?: string;
  phoneNumber?: string;
  token?: string;
}

export interface Response {
  data: {
    user: User,
  };
}
