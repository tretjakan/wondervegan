import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { options } from '@env/environment';
import { User, Response } from './auth.model';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient,
  ) { }

  /**
   * Registration route
   */
  registration(user: User) {
    return this.http.post<Response>(`${options.authApi}/users/register`, user);
  }

  /**
   * Login route
   */
  login(user: User) {
    return this.http.post<Response>(`${options.authApi}/login`, user);
  }

  /**
   * Register validation
   */
  validate(token: string) {
    return this.http.get<Response>(`${options.authApi}/login/verification/${token}`);
  }
}
