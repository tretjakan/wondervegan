
export class ProductSort {
  static priceSortTypes = {
    desc: 1,
    asc: 2,
    discountOnly: 3,
    dicountValue: 4,
  };

  /**
   * Sort by price
   */
  static sortByPrice(data, type: number) {
    if (!data.length || data.length === 1) return data;

    data.sort((a, b) => ProductSort.priceSortTypes.desc === type
      ? parseInt(a.price, 10) - parseInt(b.price, 10)
      : parseInt(b.price, 10) - parseInt(a.price, 10));
  }
}
