import { Component, ChangeDetectionStrategy, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { Store, select } from '@ngrx/store';
import { AuthComponent } from '@pages/auth/auth.component';
import { AuthState } from '@store/auth/auth.reducer';
import { selectLoggedIn, selectUser, AuthFacade } from '@store/auth/auth.facade';
import { User } from '@services/auth/auth.model';
import { LoginOut } from '@store/auth/auth.actions';
import { Router } from '@angular/router';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { PurchasesPreviewComponent } from '@pages/purchases/purchases-preview/purchases-preview.component';
import { CartFacade } from '@store/cart/cart.facade';
import { Cart } from '@store/models';


@Component({
  selector: 'ui-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ToolbarComponent implements OnInit {
  @Input() title;
  @Input() sidenav;

  password: string;
  firstName?: string;
  lastName?: string;
  email: string;
  phoneNumber?: string;
  passwordAgain?: string;
  isLoggedIn$: Observable<boolean>;
  loggedInUser$: Observable<User>;
  cartCount: number;
  cart$: Observable<number>;

  constructor(
    private router: Router,
    public dialog: MatDialog,
    private store: Store<AuthState>,
    private facade: AuthFacade,
    private _bottomSheet: MatBottomSheet,
    private cartFacade: CartFacade,
  ) {
    this.isLoggedIn$ = this.store.pipe(select(selectLoggedIn));
    this.loggedInUser$ = this.store.pipe(select(selectUser));
    this.cart$ = this.cartFacade.total$;
  }

  openBottomSheet(): void {
    this._bottomSheet.open(PurchasesPreviewComponent);
  }

  ngOnInit() {


    // const cart = sessionStorage.getItem('cart');
    // console.log('cartCount', this.cartCount);
    // if (!cart) {
    //   this.cartCount = 0;
    // } else {
    //   this.cartCount = JSON.parse(cart).length;
    // }
  }

  openDialog(): void {
    this.facade.openAuth({
      ref: this.dialog,
      component: AuthComponent,
      settings: {
        width: '400px',
        data: {
          firstName: this.firstName,
          password: this.password,
          lastName: this.lastName,
          phoneNumber: this.phoneNumber,
          email: this.email,
          passwordAgain: this.passwordAgain,
        },
      }
    });
  }

  public logOut() {
    this.store.dispatch(new LoginOut());
    if (this.router.url === '/profile') {
      this.router.navigateByUrl('/dashboard');
    }
  }
}
