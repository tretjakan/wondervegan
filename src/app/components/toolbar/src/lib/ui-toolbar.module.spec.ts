import { async, TestBed } from '@angular/core/testing';
import { ToolbarModule } from './ui-toolbar.module';

describe('UiToolbarModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ToolbarModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(ToolbarModule).toBeDefined();
  });
});
