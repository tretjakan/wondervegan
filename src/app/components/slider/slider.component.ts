import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { NouisliderComponent } from 'ng2-nouislider';


@Component({
  selector: 'slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss'],
})
export class SliderComponent implements OnInit {
  @Input() maxPrice: number;
  @Input() minPrice: number;
  @Output() filterPrice = new EventEmitter();
  @ViewChild('mrangeslider') mSliderComp: NouisliderComponent;

  max = 100;
  min = 25;
  step = 1;
  valueStart = null;
  valueEnd = null;
  range = [25, 100];

  config: any = {
    behaviour: 'drag',
    connect: true,
  };

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges() {
    if (this.maxPrice && this.minPrice) {
      this.max = this.maxPrice;
      this.valueStart = this.minPrice;
      this.valueEnd = this.maxPrice;
      this.min = this.minPrice;
      this.range = [this.minPrice, this.maxPrice];
    }
  }

  public handleChange(event: any) {
    this.filterPrice.emit({ min: event.min, max: event.max });
  }

  public onChangeEnd(e: any) {
    console.log(e)
    this.valueStart = e[0];
    this.valueEnd = e[1];
    this.handleChange({ min: this.valueStart, max: this.valueEnd });
  }

  onChange(e: any) {
    this.valueStart = e[0];
    this.valueEnd = e[1];
  }

  public handleMinChange(event: any) {
    if ((!parseInt(event.currentTarget.value, 10) || parseInt(event.currentTarget.value, 10) <= 0)
      || (parseInt(event.currentTarget.value, 10) < this.minPrice)) {
      this.handleChange({ min: this.minPrice, max: this.valueEnd });
      return;
    }
    if (parseInt(event.currentTarget.value, 10) > this.maxPrice) {
      this.valueStart = this.maxPrice;
      this.handleChange({ min: this.maxPrice, max: this.valueEnd });
      return;
    }
    this.mSliderComp.slider.set([parseInt(event.currentTarget.value, 10), this.valueEnd])
    this.handleChange({ min: event.currentTarget.value, max: this.valueEnd  });
  }


  public handleMaxChange (event: any) {
    if (!parseInt(event.currentTarget.value, 10) || parseInt(event.currentTarget.value, 10) <= 0) {
      this.valueEnd = this.maxPrice;
    }
    if (parseInt(event.currentTarget.value, 10) > this.maxPrice) {
      this.valueEnd = this.maxPrice;
    }

    this.mSliderComp.slider.set([this.valueStart, parseInt(event.currentTarget.value, 10)])
    this.handleChange({ min: this.valueStart, max: parseInt(event.currentTarget.value, 10) });
  }
}