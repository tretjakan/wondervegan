import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductsFacade } from '@store/products/products.facade';
import { Product } from '@services/products/products.model';
import { CartFacade } from '@store/cart/cart.facade';


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  private sub: any;
  product$: Observable<Product>;
  product = {};
  previousLink = null;
  rate = 4;

  constructor(
    private route: ActivatedRoute,
    private facade: ProductsFacade,
    private cartFacade: CartFacade,
  ) {
    this.product$ = facade.currentProduct$;
  }

  ngOnInit() {
    this.previousLink = `/goods/${this.route.url.value[1].path}`;
    this.sub = this.route.params.subscribe(params => {
      if (params.productId) {
        this.fetchProduct(params.productId);
      }
    });
  }

  public fetchProduct(id: number) {
    this.facade.fetchProduct(id);
  }

  public onClickBuy(product: Product) {
    this.cartFacade.addGood(product);

    let cart: any = sessionStorage.getItem('cart');

    if (!cart) {
      cart = [];
      cart.push({ id: product.productId });
      sessionStorage.setItem(`cart`, JSON.stringify(cart));
      return;
    } else {
      cart = JSON.parse(cart);
    }

    const existed = cart.find(item => item.id === product.productId);

    if (existed) {
      return;
    }
    cart.push({ id: product.productId });
    sessionStorage.setItem(`cart`, JSON.stringify(cart));
  }

// tslint:disable-next-line: use-life-cycle-interface
  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
