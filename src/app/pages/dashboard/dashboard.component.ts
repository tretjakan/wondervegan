import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { Observable } from 'rxjs';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthFacade } from '@store/auth/auth.facade';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements AfterViewInit, OnInit, OnDestroy {
  private sub: any;
  isValidationSuccess$: Observable<boolean>;

  constructor(
    private route: ActivatedRoute,
    private facade: AuthFacade,
    private _snackBar: MatSnackBar,
    private router: Router,
  ) {
    this.isValidationSuccess$ = this.facade.isValidationSuccess$;
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.sub = this.route.queryParams.subscribe((params) => {
      if (params.token) {
        const openSnackbar = (message: string, action: string) => setTimeout(
            () => this._snackBar.open(message, action, {
            duration: 5000000,
            panelClass: ['snackbar__styles']
          }),
        100);
        this.facade.validate(params.token, openSnackbar);
        this.router.navigateByUrl('dashboard');
      }
    });
  }

  public ngOnDestroy() {
    this.sub.unsubscribe();
  }

  getProjects() {
  }
}
