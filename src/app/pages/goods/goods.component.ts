import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Product } from '@services/products/products.model';
import { ProductsFacade } from '@store/products/products.facade';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';

import { LoadProduct, Filter } from '@store/models/models';


@Component({
  selector: 'app-goods',
  templateUrl: './goods.component.html',
  styleUrls: ['./goods.component.scss']
})
export class GoodsComponent implements OnInit, OnDestroy {
  products$: Observable<Product[]>;
  total$: Observable<number>;
  filters$: Observable<Filter[]>;
  minPrice$: Observable<number>;
  maxPrice$: Observable<number>;
  private sub: any;
  private id: number;

  public pageSize = '10';
  public currentPage = '0';

  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(
    private facade: ProductsFacade,
    private route: ActivatedRoute,
    private router: Router,
  ) {
    this.products$ = facade.products$;
    this.total$ = facade.total$;
    this.filters$ = facade.filters$;
    this.minPrice$ = facade.minPrice$;
    this.maxPrice$ = facade.maxPrice$;
   }

  public ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      if (params.productId) {
        this.fetchProduct(params.productId);
        return;
      }
      const id = parseInt(params.id, 10);
      this.id = id;
      if (id) {
        this.facade.getTotal(id);
        this.getProducts();
      }
    });
  }

  /**
   * Pagination changes
   */
  public onChangePage(e: any) {
    this.currentPage = e.pageIndex;
    this.pageSize = e.pageSize;

    this.getProducts();
  }

  /**
   * Get Products
   */
  public getProducts() {
    const body: LoadProduct = {
      id: this.id,
      pagination: {
        perPage: this.pageSize ,
        page: this.currentPage,
      },
    };
    this.facade.getProducts(body);
  }

  /**
   * Get Filtered Products
   */
  public getFilteredProducts(filters: Array<string>) {
    this.facade.writeFiltersToState(filters);
    this.getProducts();
  }

  /**
   * Price Filtered Products
   */

  public filterProductsByPrice(payload) {
    this.facade.writePriceToState(payload);
    this.getProducts();
  }

  /**
   * Select product
   */
  private fetchProduct(id: number) {
    this.router.navigate([`/goods/${this.route.params.value.id}`, `${id}`]);
  }

  public ngOnDestroy() {
    this.sub.unsubscribe();
  }

  public onSortProducts(payload: any) {
    this.facade.sortProducts(payload.products, payload.value);
  }

}
