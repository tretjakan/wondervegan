import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Product } from '@services/products/products.model';
import { Option } from '@store/models/models';

@Component({
  selector: 'app-goods-list',
  templateUrl: './goods-list.component.html',
  styleUrls: ['./goods-list.component.scss']
})
export class GoodsListComponent {
  @Input() products: Product[];
  @Input() currentPage: number;
  @Input() pageSize: number;
  @Input() total: number;
  @Output() selectProduct = new EventEmitter();
  @Output() paginatorChange = new EventEmitter();
  @Output() loadProducts = new EventEmitter();
  @Output() onSortProducts = new EventEmitter();

  selected = 0;
  options: Option[] = [
    { id: 0, value: 'По умолчанию' },
    { id: 1, value: 'От дешевых к дорогим' },
    { id: 2, value: 'От дорогих к дешевым' },
    { id: 4, value: 'Только товары по скидке' },
    { id: 3, value: 'По размеру скидки' },
  ];

  constructor() { }

  public selectionChange(event: any) {
    if (event.value === 0) {
      this.onSortProducts.emit({ products: this.products, value: event.value });
      this.loadProducts.emit();
    } else {
      this.onSortProducts.emit({ products: this.products, value: event.value });
    }
  }

}
