import {
  Component, OnInit, Input, Output, EventEmitter,
} from '@angular/core';
import { Filter } from '@store/models/models';
import { Product } from '@services/products/products.model';


@Component({
  selector: 'app-goods-filters',
  templateUrl: './goods-filters.component.html',
  styleUrls: ['./goods-filters.component.scss']
})
export class GoodsFiltersComponent implements OnInit {
  @Input() filters: Filter[];
  @Input() products: Product[];
  @Input() maxPrice: number;
  @Input() minPrice: number;
  @Output() filtered = new EventEmitter();
  @Output() priceFilterd = new EventEmitter();

  used = [];

  constructor() { }

  ngOnInit() {
  }

  filterPrice(payload) {
    this.priceFilterd.emit(payload);
  }

  onChange(e: any) {
    if (e.checked) {
      this.used.push(e.source.id);
      this.filtered.emit(this.used);
    } else {
      this.used = this.used.filter(filter => filter !== e.source.id);
      this.filtered.emit(this.used);
    }
  }

}
