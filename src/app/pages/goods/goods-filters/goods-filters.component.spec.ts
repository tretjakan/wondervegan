import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoodsFiltersComponent } from './goods-filters.component';

describe('GoodsFiltersComponent', () => {
  let component: GoodsFiltersComponent;
  let fixture: ComponentFixture<GoodsFiltersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoodsFiltersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoodsFiltersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
