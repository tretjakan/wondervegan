import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TreeFlatOverviewExample } from '@components/tree/tree.component';
import { MaterialModule } from '@libs/material';
import { FormsModule } from '@angular/forms';
import { GoodsRoutingModule } from './goods-routing.module';
import { GoodsComponent } from './goods.component';
import { GoodsListComponent } from './goods-list/goods-list.component';
import { GoodsFiltersComponent } from './goods-filters/goods-filters.component';
import { NouisliderModule } from 'ng2-nouislider';
import { SliderComponent } from '@components/slider/slider.component';

@NgModule({
  declarations: [
    GoodsComponent,
    TreeFlatOverviewExample,
    GoodsListComponent,
    GoodsFiltersComponent, SliderComponent,
  ],
  imports: [
    CommonModule,
    GoodsRoutingModule,
    MaterialModule,
    NouisliderModule,
    FormsModule,
  ]
})
export class GoodsModule { }
