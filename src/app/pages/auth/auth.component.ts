import { Observable } from 'rxjs';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthFacade } from '@store/auth/auth.facade';

export interface DialogData {
  password: string;
  firstName?: string;
  lastName?: string;
  email: string;
  phoneNumber: string;
  passwordAgain: string;
}

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
  isRegistration = false;
  isLoading$: Observable<boolean>;
  isError$: Observable<boolean>;
  isRegisteredSuccess$: Observable<boolean>;
  isLoggedIn$: Observable<boolean>;
  isForgotPassword = false;

  constructor(
    public dialogRef: MatDialogRef<AuthComponent>,
    private facade: AuthFacade,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
      this.isLoading$ = this.facade.isLoading$;
      this.isError$ = this.facade.isError$;
      this.isRegisteredSuccess$ = this.facade.isRegisteredSuccess$;
      this.isLoggedIn$ = this.facade.isLoggedIn$;
    }

  onNoClick(): void {
    this.dialogRef.close();
  }

  toggleType() {
    this.isRegistration = !this.isRegistration;
  }

  registerUser() {
    this.facade.registration(this.data);
  }

  tryAgain() {
    this.facade.tryAgainRegistration();
  }

  login() {
    this.facade.login(this.data, this.dialogRef);
  }

  logOut() {
    this.facade.logOut();
  }

  onForgotPassword() {
    this.isForgotPassword = true;
  }

  ngOnInit() {
  }

}
