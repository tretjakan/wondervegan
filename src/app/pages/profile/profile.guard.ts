import {
  CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot,
  Router,
} from '@angular/router';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProfileGuard implements CanActivate{

  constructor(
    private router: Router
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    if (!user || !user.token) {
      this.router.navigate(['/dashboard']);
      return false;
    }
    return true;
  }
}
