import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Category } from '@services/categories/categories.model';

@Component({
  selector: 'app-categories-list',
  templateUrl: './categories-list.component.html',
  styleUrls: ['./categories-list.component.scss']
})
export class CategoriesListComponent implements OnInit {
  @Input() categories: Category[];
  @Output() selected = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

}
