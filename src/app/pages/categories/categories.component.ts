import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Category } from '@services/categories/categories.model';
import { CategoriesFacade } from '@store/categories/category.facade';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {
  categories$: Observable<Category[]>;

  constructor(
    private facade: CategoriesFacade,
    private router: Router
  ) {
    this.categories$ = facade.categories$;
   }

  ngOnInit() {
    this.getProducts();
  }

  getProducts() {
    this.facade.getCategories();
  }

  onSelect(category: Category) {
    this.facade.selectCategory(category);
    this.router.navigate(['/goods', `${category.alias}`]);
  }

}
