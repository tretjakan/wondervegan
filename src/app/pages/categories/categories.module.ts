import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoriesListComponent } from './categories-list/categories-list.component';
import { MaterialModule } from '@libs/material';

@NgModule({
  declarations: [CategoriesListComponent],
  imports: [
    CommonModule,
    MaterialModule,
  ]
})
export class CategoriesModule { }
