import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchasesPreviewComponent } from './purchases-preview.component';

describe('PurchasesPreviewComponent', () => {
  let component: PurchasesPreviewComponent;
  let fixture: ComponentFixture<PurchasesPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchasesPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchasesPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
