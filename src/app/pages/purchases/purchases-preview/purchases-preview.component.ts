import { Component, OnInit } from '@angular/core';
import { CartFacade } from '@store/cart/cart.facade';
import { map } from 'rxjs/operators';
import { Cart } from '@store/models';


@Component({
  selector: 'app-purchases-preview',
  templateUrl: './purchases-preview.component.html',
  styleUrls: ['./purchases-preview.component.scss']
})
export class PurchasesPreviewComponent implements OnInit {
  goods$: any;

  constructor(
    private cartFacade: CartFacade,
  ) {
    this.goods$ = this.cartFacade.cart$.pipe(map(val => val.map(item => ({
        header: {
          title: item.name,
          subtitle: item.price,
        },
        content: {
          description: item.description,
          count: this.cartFacade.cartCount$,
        },
        actions: {
          name: 'Delete',
        },
        img: item.imageUrl,
        id: item.productId,
      }))
    ));
  }

  remove(item: Cart) {
    this.cartFacade.deleteGood(item);
  }

  increase(id: number) {
    this.cartFacade.increaseCount(id);
  }

  decrease(id: number) {
    this.cartFacade.reduceCount(id);
  }

  ngOnInit() {}
}
