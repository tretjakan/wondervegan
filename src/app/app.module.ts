import { BrowserModule, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgxLoadingModule } from 'ngx-loading';
import { BarRatingModule } from "ngx-bar-rating";
import { HttpClientModule } from '@angular/common/http';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GestureConfig, MatBottomSheetModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { StateModule } from '@store/store';

import { MaterialModule } from '@libs/material';
import { ToolbarModule } from '@components/toolbar/src';
import { FooterComponent } from '@components/footer/footer.component';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CategoriesComponent } from './pages/categories/categories.component';
import { DeliveryComponent } from './pages/delivery/delivery.component';
import { AboutComponent } from './pages/about/about.component';
import { CategoriesListComponent } from './pages/categories/categories-list/categories-list.component';
import { ProductComponent } from './pages/product/product.component';
import { AuthComponent } from './pages/auth/auth.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { ProductDescriptionComponent } from './pages/product/product-description/product-description.component';
import { PurchasesComponent } from './pages/purchases/purchases.component';
import { PurchasesPreviewComponent } from './pages/purchases/purchases-preview/purchases-preview.component';
import { ProductPurchaseComponent } from './components/product-purchase/product-purchase.component';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    CategoriesComponent,
    DeliveryComponent,
    AboutComponent,
    CategoriesListComponent,
    ProductComponent,
    AuthComponent,
    ProfileComponent,
    ProductDescriptionComponent,
    PurchasesComponent,
    PurchasesPreviewComponent,
    ProductPurchaseComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    ToolbarModule,
    RouterModule,
    MaterialModule,
    HttpClientModule,
    StateModule,
    FormsModule,
    NgxLoadingModule.forRoot({}),
    BarRatingModule,
    MatBottomSheetModule,
  ],
  providers: [
    { provide: HAMMER_GESTURE_CONFIG, useClass: GestureConfig },
  ],
  bootstrap: [AppComponent],
  entryComponents: [AuthComponent, PurchasesPreviewComponent]
})
export class AppModule { }
